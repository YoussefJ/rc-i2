function addItem() {
    var ul = document.getElementById('template-poi-items');
    var li = document.createElement("li");
    li.appendChild(document.createTextNode("new item"));
    ul.appendChild(li);
}

function addElement() {
    var elements = document.getElementById('elements');
    var newElement = document.createElement('div');
    newElement.classList.add('element');
    elementFlex = document.createElement('div');
    var title = document.createElement('h5');
    title.innerHTML = 'Click to add title';
    title.setAttribute('contenteditable', 'true');
    var subtitle = document.createElement('p');
    subtitle.innerHTML = 'Click to add subtitle';
    subtitle.setAttribute('contenteditable', 'true');

    elementFlex.appendChild(title);
    elementFlex.appendChild(subtitle);
    newElement.appendChild(elementFlex);

    trashButton = document.createElement('i')
    trashButton.classList.add('fa-solid');
    trashButton.classList.add('fa-trash-can');
    trashButton.addEventListener('click', remove);

    newElement.appendChild(trashButton);
    elements.appendChild(newElement);
}

function remove(event) {
    deleteElement(event.target)
}

function deleteElement(element) {
    document.getElementById('elements').removeChild(element.parentNode);
}

function addImage(event) {
    var image = document.getElementById('image-to-add')
    image.src = URL.createObjectURL(event.target.files[0]);
}