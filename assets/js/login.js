const users = [
    {
        username: 'admin',
        password: 'admin',
    },
    {
        username: 'testVneuron',
        password: 'testVneuron',
    },
];

function loginUser(e) {
    e.preventDefault();
    console.log("here")
    username = document.getElementById('username').value;
    password = document.getElementById('password').value;
    if (username === '' || password === '') {
        text = 'Credentials must not be empty.';
        document.querySelector('.login-error').innerHTML = text;
        return;
    }
    if (password.length < 4) {
        text = 'Password must be at least 6 characters long.';
        document.querySelector('.login-error').innerHTML = text;
        return;
    }
    for (let i = 0; i < users.length; i++) {
        if (users[i].username === username && users[i].password === password) {
            text = ''
            window.location.href = 'homepage.html';
            return true;
        }
    }
    text = 'Invalid credentials.';
    document.querySelector('.login-error').innerHTML = text;
    return false;
}



